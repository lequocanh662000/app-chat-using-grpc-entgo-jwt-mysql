package config

const (
	GrpcServerHost      = "127.0.0.1"
	GrpcServerPort      = 50052
	WebSocketServerPort = 8080
)
