// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package chat

import "log"

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// A list of room
	room []*Room

	// Active clients's name
	active_clients []string

	// Active rooms's name
	active_rooms []string
}

type Room struct {
	room_id        string
	room_broadcast chan []byte
	members        []*Client
}

func (r *Room) doBroadcast(message []byte) {
	for _, member := range r.members {
		log.Println("send to ->", member)
		member.send <- message
	}
}

func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		// broadcast
		clients: make(map[*Client]bool),
		// // private rooms
		room: make([]*Room, 0, 256),
	}
}

type Mess struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Room_id  string `json:"room_id"`
	Content  string `json:"content"`
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
