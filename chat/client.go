// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package chat

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	pb "gitlab.com/lequocanh662000/app-chat-using-grpc-entgo-jwt-mysql/api"
	"gitlab.com/lequocanh662000/app-chat-using-grpc-entgo-jwt-mysql/pkg/ent"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	// database
	db *ent.Client

	// room controller
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte

	username string
}

// readPump: pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		var temp pb.Message
		err = proto.Unmarshal(message, &temp)
		if err != nil {
			log.Println("Get trouble when Unmarshal []byte message")
		}
		if c.username == "" {
			switch {
			case temp.GetChatRequest() != nil:
				isExist := false
				for _, name := range c.hub.active_clients {
					if name == temp.GetChatRequest().User.Username {
						isExist = true
					}
				}
				if !isExist {
					c.hub.active_clients = append(c.hub.active_clients, temp.GetChatRequest().User.Username)
				}
			case temp.GetJoinRoomRequest() != nil:
				isExist := false
				for _, name := range c.hub.active_clients {
					if name == temp.GetJoinRoomRequest().User.Username {
						isExist = true
					}
				}
				if !isExist {
					c.hub.active_clients = append(c.hub.active_clients, temp.GetJoinRoomRequest().User.Username)
				}
			case temp.GetLoginRequest() != nil:
				isExist := false
				for _, name := range c.hub.active_clients {
					if name == temp.GetLoginRequest().User.Username {
						isExist = true
					}
				}
				if !isExist {
					c.hub.active_clients = append(c.hub.active_clients, temp.GetLoginRequest().User.Username)
				}
			}
		}
		// Handle with Database
		c.databaseHelper(&temp) // Log all kinds of message into database
		c.ReplyHelper(&temp)
		c.UpdateHubHelper(&temp)
	}
}

// writePump: pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is 0started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}

			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func ServeWs(hub *Hub, db *ent.Client, w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, db: db, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client
	log.Println("Registered client: ", client)
	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

///////////////////// Database Helper //////////////////////////////////
func (c *Client) databaseHelper(temp *pb.Message) {
	if temp.GetLoginRequest() != nil {
		// Login Request
		c.databaseHelper_Login(temp)
	} else if temp.GetJoinRoomRequest() != nil {
		// Join Room Request
		c.databaseHelper_JoinRoom(temp)
	} else if temp.GetChatRequest() != nil {
		// Chat Request
		c.databaseHelper_Chat(temp)
	}
}

func (c *Client) databaseHelper_Login(temp *pb.Message) {
	c.username = temp.GetLoginRequest().User.Username
	request := temp.GetLoginRequest()
	// put message's data into database
	u, err := c.db.User.
		Create().
		SetTime(time.Now().String()).
		SetFrom(request.User.Username).
		SetPassword(request.GetUser().Password).
		SetContent("").
		Save(context.Background())
	if err != nil {
		fmt.Println("Failed creating use`: %w", err)
	} else {
		fmt.Println("User was created: ", u)
	}
}

func (c *Client) databaseHelper_JoinRoom(temp *pb.Message) {
	c.username = temp.GetJoinRoomRequest().User.Username
	request := temp.GetJoinRoomRequest()
	// put message's data into database
	u, err := c.db.User.
		Create().
		SetTime(time.Now().String()).
		SetFrom(request.User.String()).
		SetPassword(request.User.Password).
		SetRoomID(request.RoomId).
		SetContent("").
		Save(context.Background())
	if err != nil {
		fmt.Println("Failed creating use`: %w", err)
	} else {
		fmt.Println("User was created: ", u)
	}
}

func (c *Client) databaseHelper_Chat(temp *pb.Message) {
	c.username = temp.GetChatRequest().User.Username
	request := temp.GetChatRequest()
	// put message's data into database
	u, err := c.db.User.
		Create().
		SetTime(time.Now().String()).
		SetFrom(request.User.String()).
		SetPassword(request.User.Password).
		SetRoomID(request.RoomId).
		SetContent(request.Msg).
		Save(context.Background())
	if err != nil {
		fmt.Println("Failed creating use`: %w", err)
	} else {
		fmt.Println("Message was received: ", u)
	}
}

////////////////////// Reply_Helper ////////////////////////////////
func (c *Client) ReplyHelper(temp *pb.Message) {
	switch {
	case temp.GetChatRequest() != nil:
		c.chatRequest_Helper(temp)
	case temp.GetJoinRoomRequest() != nil:
		c.joinRoomRequest_Helper(temp)
	case temp.GetLoginRequest() != nil:
		c.loginRequest_Helper(temp)
	}
}

func (c *Client) chatRequest_Helper(temp *pb.Message) {
	room_flag := false
	// Check room is exist in Hub or not
	for i, room := range c.hub.room {
		// If Room is already exist in Hub
		if room.room_id == temp.GetChatRequest().RoomId {
			// check Member is exist in Room or not
			var isMember bool = false
			for _, x := range c.hub.room[i].members {
				if x == c {
					isMember = true
					break
				}
			}
			// If member is not in room -> add member into room
			if isMember == false {
				c.hub.room[i].members = append(c.hub.room[i].members, c)
			}
			// Convert from protobuf to []byte
			var response = pb.Message{
				Payload: &pb.Message_ChatReply{
					ChatReply: &pb.ChatReply{
						Error:       0,
						From:        temp.GetChatRequest().GetUser().Username,
						Msg:         temp.GetChatRequest().Msg,
						Time:        time.Now().String(),
						ActiveUsers: c.hub.active_clients,
						ActiveRooms: c.hub.active_rooms,
					},
				},
			}
			converted_response, err := proto.Marshal(&response)
			if err != nil {
				log.Println("Can't convert from protobuf to []byte")
			}
			c.hub.room[i].room_broadcast <- converted_response
			c.hub.room[i].doBroadcast(converted_response)
			room_flag = true
		}
	}
	// Can't find Room_ID || Room_ID = ""
	if room_flag == false {
		// Can't find Room_ID -> Create a new room
		if temp.GetChatRequest().RoomId != "" && temp.GetChatRequest().User.Username != "" {
			log.Println("New room was created: ")
			c.hub.room = append(c.hub.room, &Room{room_id: temp.GetChatRequest().RoomId, room_broadcast: make(chan []byte, 256), members: []*Client{}})
			// Update Active_Rooms in Hub
			c.hub.active_rooms = append(c.hub.active_rooms, temp.GetJoinRoomRequest().RoomId)
			// Add user into that created room
			last_index := len(c.hub.room) - 1
			c.hub.room[last_index].members = append(c.hub.room[last_index].members, c)
			// After creating new room -> broadcast message into that room
			var response = pb.Message{
				Payload: &pb.Message_ChatReply{
					ChatReply: &pb.ChatReply{
						Error: 0,
						Msg:   "[System] Room " + temp.GetChatRequest().RoomId + " was created \n" + temp.GetChatRequest().Msg,
						Time:  time.Now().String(),
					},
				},
			}
			converted_response, err := proto.Marshal(&response)
			if err != nil {
				log.Println("Can't convert from protobuf to []byte")
			}
			c.hub.room[last_index].room_broadcast <- converted_response
			c.hub.room[last_index].doBroadcast(converted_response)

		} else {
			// Room_ID = ""
			var response = pb.Message{
				Payload: &pb.Message_ChatReply{
					ChatReply: &pb.ChatReply{
						Error:       0,
						From:        temp.GetChatRequest().GetUser().Username,
						Msg:         temp.GetChatRequest().Msg,
						Time:        time.Now().String(),
						ActiveUsers: c.hub.active_clients,
						ActiveRooms: c.hub.active_rooms,
					},
				},
			}
			log.Println("No Room is specified -> Broadcast message to everyone")
			converted_response, err := proto.Marshal(&response)
			if err != nil {
				log.Println("Can't convert from protobuf to []byte")
			}
			c.hub.broadcast <- converted_response
		}
	}
}

func (c *Client) joinRoomRequest_Helper(temp *pb.Message) {
	room_flag := false
	// Check room is exist in Hub or not
	for i, room := range c.hub.room {
		// If Room is already exist in Hub
		if room.room_id == temp.GetJoinRoomRequest().RoomId {
			// check Member is exist in Room or not
			var isMember bool = false
			for _, x := range c.hub.room[i].members {
				if x == c {
					isMember = true
					break
				}
			}
			// If member is not in room -> add member into room
			if isMember == false {
				c.hub.room[i].members = append(c.hub.room[i].members, c)
			}
			// Convert from protobuf to []byte
			msg := ""
			if isMember == false {
				msg = "you have just added into room"
			} else {
				msg = "you are already in room"
			}
			// Check Active Users && Active Rooms
			var response = pb.Message{
				Payload: &pb.Message_JoinRoomReply{
					JoinRoomReply: &pb.JoinRoomReply{
						Error:       0,
						Msg:         msg,
						ActiveUsers: c.hub.active_clients,
						ActiveRooms: c.hub.active_rooms,
					},
				},
			}
			converted_response, err := proto.Marshal(&response)
			if err != nil {
				log.Println("Can't convert from protobuf to []byte")
			}
			c.hub.room[i].room_broadcast <- converted_response
			c.hub.room[i].doBroadcast(converted_response)
			room_flag = true
		}
	}
	// If room is not exist in Hub -> add Room into Hub
	if room_flag == false {
		log.Println("New room was created: ")
		c.hub.room = append(c.hub.room, &Room{room_id: temp.GetJoinRoomRequest().RoomId, room_broadcast: make(chan []byte, 256), members: []*Client{}})
		// Update Active_Rooms in Hub
		c.hub.active_rooms = append(c.hub.active_rooms, temp.GetJoinRoomRequest().RoomId)
		// Add user into that created room
		last_index := len(c.hub.room) - 1
		c.hub.room[last_index].members = append(c.hub.room[last_index].members, c)
		// After creating new room -> broadcast message into that room
		var response = pb.Message{
			Payload: &pb.Message_JoinRoomReply{
				JoinRoomReply: &pb.JoinRoomReply{
					Error:       0,
					Msg:         "[System] Room " + temp.GetJoinRoomRequest().RoomId + " was created \n",
					ActiveUsers: c.hub.active_clients,
					ActiveRooms: c.hub.active_rooms,
				},
			},
		}
		converted_response, err := proto.Marshal(&response)
		if err != nil {
			log.Println("Can't convert from protobuf to []byte")
		}
		c.hub.room[last_index].room_broadcast <- converted_response
		c.hub.room[last_index].doBroadcast(converted_response)
	}
}

func (c *Client) loginRequest_Helper(temp *pb.Message) {
	msg := "null"
	// handle database (not yet)
	var response = pb.Message{
		Payload: &pb.Message_LoginReply{
			LoginReply: &pb.LoginReply{
				Err: 0,
				Msg: msg,
			},
		},
	}
	log.Println("Broadcast message to everyone")
	converted_response, err := proto.Marshal(&response)
	if err != nil {
		log.Println("Can't convert from protobuf to []byte")
	}
	c.hub.broadcast <- converted_response
}

//////////////////////// Update Hub_Helper //////////////////////////////////
func (c *Client) UpdateHubHelper(temp *pb.Message) {

}
