package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("time").Default("unknown"),
		field.String("from").Default("unknown"),
		field.String("password").Default("unknown"),
		field.String("room_id").Default("unknown"),
		field.String("content"),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return nil
}
