// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// import (
// 	"flag"
// 	"log"
// 	"net/http"

// 	"gitlab.com/lequocanh662000/app-chat-using-grpc-entgo-jwt-mysql/chat"
// )

// var addr = flag.String("addr", ":8080", "http service address")

// func serveFrontend(w http.ResponseWriter, r *http.Request) {
// 	log.Println(r.URL)
// 	if r.URL.Path != "/" {
// 		http.Error(w, "Not found", http.StatusNotFound)
// 		return
// 	}
// 	if r.Method != http.MethodGet {
// 		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
// 		return
// 	}
// 	http.ServeFile(w, r, "static/home.html")
// }

// func main() {
// 	flag.Parse()
// 	hub := chat.NewHub()
// 	go hub.Run()
// 	http.HandleFunc("/", serveFrontend)
// 	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
// 		chat.ServeWs(hub, w, r)
// 	})
// 	err := http.ListenAndServe(*addr, nil)
// 	if err != nil {
// 		log.Fatal("ListenAndServe: ", err)
// 	}

// }
