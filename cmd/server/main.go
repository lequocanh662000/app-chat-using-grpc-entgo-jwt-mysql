package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/lequocanh662000/app-chat-using-grpc-entgo-jwt-mysql/chat"
	"gitlab.com/lequocanh662000/app-chat-using-grpc-entgo-jwt-mysql/pkg/ent"
)

func main() {
	db_main_host := os.Getenv("DB_HOST")
	fmt.Println(db_main_host)
	database, err := ent.Open("mysql", fmt.Sprintf("root:P@ssw0rd@tcp(%s:3306)/app1?parseTime=True", db_main_host))
	// database, err := ent.Open("mysql", fmt.Sprintf("root:P@ssw0rd@tcp(%s:13306)/app1?parseTime=True", db_main_host))
	if err != nil {
		log.Fatalf("failed opening connection to mySQL: %v", err)
	}
	defer database.Close()
	// Run the auto migration tool.
	fmt.Printf("db_main_host: %s", db_main_host)
	if err := database.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	//////////////////////////////

	hub := chat.NewHub()
	go hub.Run()

	fmt.Print("Created Hub successfully !\n")
	// for running Docker image
	pwd, _ := os.Getwd()
	fileChatClient := http.FileServer(http.Dir(pwd + "/chat_app/public"))
	http.Handle("/", http.StripPrefix(strings.TrimRight("/", "/"), fileChatClient))
	// for Local
	// fileChatClient := http.FileServer(http.Dir(pwd + "/public"))
	// http.Handle("/", http.StripPrefix(strings.TrimRight("/", "/"), fileChatClient))

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		chat.ServeWs(hub, database, w, r)
	})

	err = http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatalf("ListenAndServe: %v", err)
	}
}

var addr = flag.String("addr", ":8080", "http service address")

// func serveFrontend(w http.ResponseWriter, r *http.Request) {
// 	log.Println(r.URL)
// 	if r.URL.Path != "/" {
// 		http.Error(w, "Not found", http.StatusNotFound)
// 		return
// 	}
// 	if r.Method != http.MethodGet {
// 		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
// 		return
// 	}
// 	http.ServeFile(w, r, "static/home.html")
// }
