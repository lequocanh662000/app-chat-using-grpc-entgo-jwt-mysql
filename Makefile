IMAGE = chat-app
AWS_PROFILE = default
AWS_REGION = ap-southeast-1
AWS_ACCOUNT_ID = 545685912171
AWS_SERVER = $(AWS_ACCOUNT_ID).dkr.ecr.$(AWS_REGION).amazonaws.com
TAG_LATEST = $(AWS_SERVER)/$(IMAGE):latest

build:
	docker build --cache-from $(TAG_LATEST) -t $(TAG_LATEST) . 

login:
	aws ecr get-login-password --region $(AWS_REGION) --profile $(AWS_PROFILE) | docker login --username AWS --password-stdin $(AWS_SERVER)

push: build login
	docker push $(TAG_LATEST)	