FROM golang:1.19beta1-alpine3.16

# Make app folder
RUN mkdir /app

# Set destination for COPY
WORKDIR /app

#COPY all files -> then DOWNLOAD GO MODULES 
COPY . /app/chat_app
RUN cd /app/chat_app && go mod download all
#Build
RUN cd /app/chat_app/cmd/server && go build -o /server

EXPOSE 8080

# CMD ["/server"]

COPY wait-for-mysql.sh /wait-for-mysql.sh
RUN chmod +x /wait-for-mysql.sh
RUN apk add --no-cache bash

CMD [ "/wait-for-mysql.sh","mysql-server:3306", "--", "/server" ]
